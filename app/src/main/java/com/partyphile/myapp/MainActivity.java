package com.partyphile.myapp;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by jose on 4/16/16.
 */
public class MainActivity extends Activity implements View.OnClickListener{

    private ImageView iv_image_one;
    private ImageView iv_image_two;
    private ImageView iv_image_three;
    private ImageView iv_image_four;
    private ImageView iv_image_five;
    private ImageView iv_image_six;
    private ImageView iv_image_seven;
    private ImageView iv_image_eight;
    private ImageView iv_image_nine;
    private ImageView iv_image_ten;
    private ImageView iv_image_eleven;
    private ImageView iv_image_twelve;
    private ImageView iv_image_thirteen;
    private ImageView iv_image_fourteen;
    private ImageView iv_image_fifteen;
    private ImageView iv_image_sixteen;

    private int selectedId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        selectedId = -1;

        iv_image_one = (ImageView) findViewById(R.id.iv_image_one);
        iv_image_one.setOnClickListener(this);

        iv_image_two = (ImageView) findViewById(R.id.iv_image_two);
        iv_image_two.setOnClickListener(this);

        iv_image_three = (ImageView) findViewById(R.id.iv_image_three);
        iv_image_three.setOnClickListener(this);

        iv_image_four = (ImageView) findViewById(R.id.iv_image_four);
        iv_image_four.setOnClickListener(this);

        iv_image_five = (ImageView) findViewById(R.id.iv_image_five);
        iv_image_five.setOnClickListener(this);

        iv_image_six = (ImageView) findViewById(R.id.iv_image_six);
        iv_image_six.setOnClickListener(this);

        iv_image_seven = (ImageView) findViewById(R.id.iv_image_seven);
        iv_image_seven.setOnClickListener(this);

        iv_image_eight = (ImageView) findViewById(R.id.iv_image_eight);
        iv_image_eight.setOnClickListener(this);

        iv_image_nine = (ImageView) findViewById(R.id.iv_image_nine);
        iv_image_nine.setOnClickListener(this);

        iv_image_ten = (ImageView) findViewById(R.id.iv_image_ten);
        iv_image_ten.setOnClickListener(this);

        iv_image_eleven = (ImageView) findViewById(R.id.iv_image_eleven);
        iv_image_eleven.setOnClickListener(this);

        iv_image_twelve = (ImageView) findViewById(R.id.iv_image_twelve);
        iv_image_twelve.setOnClickListener(this);

        iv_image_thirteen = (ImageView) findViewById(R.id.iv_image_thirteen);
        iv_image_thirteen.setOnClickListener(this);

        iv_image_fourteen = (ImageView) findViewById(R.id.iv_image_fourteen);
        iv_image_fourteen.setOnClickListener(this);

        iv_image_fifteen = (ImageView) findViewById(R.id.iv_image_fifteen);
        iv_image_fifteen.setOnClickListener(this);

        iv_image_sixteen = (ImageView) findViewById(R.id.iv_image_sixteen);
        iv_image_sixteen.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        ImageView iv_clicked = (ImageView) v;
        int clickedId = v.getId();

        if (selectedId == -1) {
            selectedId = clickedId;
            Toast.makeText(this, "Please select another", Toast.LENGTH_SHORT).show();
            return;
        }

        if (selectedId != -1 && clickedId == selectedId) {
            selectedId = -1;
            Toast.makeText(this, "Unselected", Toast.LENGTH_SHORT).show();
            return;
        }

        ImageView iv_selected = (ImageView) findViewById(selectedId);
        if (iv_selected.getDrawable().getConstantState().equals(iv_clicked.getDrawable().getConstantState())) {
            selectedId = -1;
            iv_clicked.setVisibility(View.INVISIBLE);
            iv_selected.setVisibility(View.INVISIBLE);
            Toast.makeText(this, "Matched! :D", Toast.LENGTH_SHORT).show();
        } else {
            selectedId = -1;
            Toast.makeText(this, "Incorrect :(", Toast.LENGTH_SHORT).show();
        }
    }
}
